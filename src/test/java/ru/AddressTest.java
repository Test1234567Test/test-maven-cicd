package ru;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AddressTest {

    @Test
    void address() {
        Address address = new Address();

        assertTrue(address.address().contains("Address"));
    }
}