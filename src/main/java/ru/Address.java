package ru;

import java.net.InetAddress;

public class Address {

    public String address(){
        try {
            return "Address " + InetAddress.getLocalHost().getHostAddress();
        }catch (Exception e){
            return "Exception while get address";
        }
    }
}
