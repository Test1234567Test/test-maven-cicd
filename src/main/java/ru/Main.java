package ru;

public class Main {
    public static void main(String[] args) {
        greetings().greetings();
        System.out.println(getAddress().address());
    }

    private static Address getAddress() {
        return new Address();
    }

    private static Greetings greetings() {
        return new Greetings("Hello");
    }
}
