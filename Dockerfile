FROM openjdk:11-slim
ADD /target/maven-test-1.0-SNAPSHOT.jar demo.jar
ENTRYPOINT ["java","-jar","demo.jar"]
